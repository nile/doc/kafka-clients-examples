#!/usr/bin/python3
#
# Simple example with confluent_kafka and kerberos
#
from confluent_kafka import Producer
import config
import datetime

p = Producer({
    'bootstrap.servers': config.BOOTSTRAP_SERVERS,
    'group.id': config.GROUP_ID,
    'ssl.ca.location': config.CACERTS,
    'security.protocol': 'SASL_SSL',
    'sasl.kerberos.kinit.cmd': 'true',
    'auto.offset.reset': 'latest',
    'enable.auto.offset.store': True,
    'sasl.kerberos.principal': config.PRINCIPAL,
    'log_level': 0
})



def delivery_report(err, msg):
    """ Called once for each message produced to indicate delivery result.
        Triggered by poll() or flush(). """
    if err is not None:
        print('Message delivery failed: {}'.format(err))
    else:
        print('Message delivered to {} [{}]'.format(msg.topic(), msg.partition()))


p.poll(0)
p.produce(config.TOPIC
         ,"this is a test {timestamp}".
             format(timestamp=datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S")) 
         , callback=delivery_report)

p.flush()

