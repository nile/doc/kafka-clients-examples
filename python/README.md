Kafka Python Clients Examples
=============================

For these examples to work, `config.py` must be modified to:
* change the Kafka cluster alias (`<alias>`)
* change the topic name (`<topic>`)
* change the user name (`<username>`)

`producer-cached.py` and `consumer-cached.py` use cached credentials.

`producer-keytab.py` and `consumer-keytab.py` use keytab credentials.
See the Java examples for instructions on how to create the keytab.

Note: these examples have been tested with Python 3.9.

Setup
=====

`confluent-kafka` with Kerberos support must be installed.

The version on PyPI ([confluent-kafka/](https://pypi.org/project/confluent-kafka/))
does *not* have Kerberos support built in.

Note: in the instructions below, if the machine is Puppet managed, you must
replace `/etc/yum.repos.d/` by `/etc/yum-puppet.repos.d/`.

For CentOS 7.x
===============

```
# NAME="db7-stable"
# yum-config-manager --add-repo "http://linuxsoft.cern.ch/internal/repos/${NAME}/\$basearch/os"
# FILE=$(ls -tr /etc/yum.repos.d/ | tail -n1)
# sed -i "s/\[.*\]/[${NAME}]\npriority=1/" /etc/yum.repos.d/${FILE}
# sed -i "s/name=.*/name=${NAME}/" /etc/yum.repos.d/${FILE}
# yum install cerndb-sw-python3-confluent_kafka
```

For CentOS Stream 8.x
=====================

Like for CentOS 7.x but substitute the Koji tag `db7-stable` with `db8s-stable`.

For AlmaLinux 9.x
=================

```
# NAME="nile9al-stable"
# yum-config-manager --add-repo "http://linuxsoft.cern.ch/internal/repos/${NAME}/\$basearch/os"
# FILE=$(ls -tr /etc/yum.repos.d/ | tail -n1)
# sed -i "s/\[.*\]/[${NAME}]\npriority=1/" /etc/yum.repos.d/${FILE}
# sed -i "s/name=.*/name=${NAME}/" /etc/yum.repos.d/${FILE}
# dnf install nile-python3-confluent_kafka
```

For Red Hat Enterprise Linux 9.x
================================

Like for AlmaLinux 9.x but substitute the Koji tag `nile9al-stable` with `nile9el-stable`.

For Other Operating Systems
===========================

Follow the instructions in `../scripts/create-confluent-kafka-python`.
