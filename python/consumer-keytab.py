#!/usr/bin/python3
from confluent_kafka import Consumer, KafkaError
import config

c = Consumer({
    'bootstrap.servers': config.BOOTSTRAP_SERVERS,
    'group.id': config.GROUP_ID,
    'ssl.ca.location': config.CACERTS,
    'security.protocol': 'SASL_SSL',
    'sasl.kerberos.kinit.cmd': 'true',
    'auto.offset.reset': 'latest',
    'enable.auto.offset.store': True,
    'sasl.kerberos.principal': config.PRINCIPAL,
    'log_level': 0
})

c.subscribe([config.TOPIC])

while True:
    msg = c.poll(1.0)
    if msg is None:
        continue
    if msg.error():
        if msg.error().code() == KafkaError._PARTITION_EOF:
            continue
        else:
            print(msg.error())
            break
    print('Received message: {}'.format(msg.value().decode('utf-8')))

c.close()


