KAFKA_CLUSTER = "<alias>"
TOPIC = "<topic>"
GROUP_ID = "kafka-clients-examples-python"
PRINCIPAL = "<username>@CERN.CH"
KEYTAB = "client.keytab"
CACERTS = "/etc/pki/tls/certs/"

import socket
import string

BOOTSTRAP_SERVERS = ",".join(
    map(lambda x: x + ":9093", sorted([
        (socket.gethostbyaddr(i))[0] for i in (socket.gethostbyname_ex(KAFKA_CLUSTER + ".cern.ch"))[2]
    ]))
)
