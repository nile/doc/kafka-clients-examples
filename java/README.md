Kafka Java Clients Examples
===========================

These examples have been taken from https://developer.confluent.io/get-started/java/
and modified to use Kerberos. Please refere to the website above for more details
on how to compile and execute them.

Note: these examples have been tested with Java 11.

Setup
=====

* change the hard-coded topic name (`<topic>`) in the Java source files
* compile with `gradle build`
* package with `gradle shadowJar`
* create the truststore with `../scripts/create-truststore`
* change the bootstrap servers (`<servers>`) in `main.conf`

For this last step, you can use `../scripts/get-bootstrap-servers <alias>` to get the
list of bootstrap servers from a Kafka cluster alias such as `kafka-gp`.

Running With Cached Credentials
===============================

```
java -Djava.security.auth.login.config=jaas-cached.conf -cp build/libs/kafka-clients-examples-java-0.1.jar examples.ConsumerExample main.conf
java -Djava.security.auth.login.config=jaas-cached.conf -cp build/libs/kafka-clients-examples-java-0.1.jar examples.ProducerExample main.conf
```

Running With Keytab Credentials
===============================

* change the user name (`<username>`) in both `../scripts/create-keytab` and `jaas-keytab.conf`
* change the password (`<password>`) in `../scripts/create-keytab`
* create the keytab with `../scripts/create-keytab`

```
java -Djava.security.auth.login.config=jaas-keytab.conf -cp build/libs/kafka-clients-examples-java-0.1.jar examples.ConsumerExample main.conf
java -Djava.security.auth.login.config=jaas-keytab.conf -cp build/libs/kafka-clients-examples-java-0.1.jar examples.ProducerExample main.conf
```
