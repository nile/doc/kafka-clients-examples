Kafka Clients Examples
======================

Here you can find scripts and source files for Java and Python Kafka clients examples.

More information can be found in the
[Nile User Guide](https://nile-user-guide.docs.cern.ch/),
especially in the
[Authentication section](https://nile-user-guide.docs.cern.ch/general_purpose/clients/authentication/).
