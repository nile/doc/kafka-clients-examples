Kafka Clients Examples Scripts
==============================

Here you can find scripts that could be useful when playing with Kafka clients
examples.

`create-confluent-kafka-python`: create a virtual environment with `confluent-kafka-python` installed

`create-keytab`: create the Kerberos keytab used by Kafka clients

`create-truststore`: create the JKS truststore used by Kafka Java clients

`get-bootstrap-servers`: get the list of bootstrap servers from DNS aliases
